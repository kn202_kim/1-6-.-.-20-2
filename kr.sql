create database kn202_kim
use kn202_kim


CREATE TABLE [route]
(
 [route_id]          int NOT NULL ,
 [num_of_stops]      int NOT NULL ,
 [num_of_passangers] int NOT NULL ,
 [num_of_cars]       int NOT NULL ,


 CONSTRAINT [PK_12] PRIMARY KEY CLUSTERED ([route_id] ASC)
);

CREATE TABLE [path]
(
 [path_id]  int NOT NULL ,
 [route_id] int NOT NULL ,
 [start]    char(50) NOT NULL ,
 [finish]   char(50) NOT NULL ,
 [distance] int NOT NULL ,


 CONSTRAINT [PK_18] PRIMARY KEY CLUSTERED ([path_id] ASC),
 CONSTRAINT [FK_62] FOREIGN KEY ([route_id])  REFERENCES [route]([route_id])
);

CREATE TABLE [transport_name]
(
 [transport_id] int NOT NULL ,
 [name]         char(10) NOT NULL ,
 [route_id]     int NOT NULL ,
 [avg_speed]    int NOT NULL ,
 [coast]        int NOT NULL ,
 [num]          int NOT NULL ,


 CONSTRAINT [PK_5] PRIMARY KEY CLUSTERED ([transport_id] ASC),
 CONSTRAINT [FK_56] FOREIGN KEY ([route_id])  REFERENCES [route]([route_id])
);



insert into route (route_id, num_of_stops, num_of_cars, num_of_passangers)
values (4, 14, 4, 115), (1, 12, 4, 100), (2, 10, 3, 80), (3, 20, 8, 130)

insert into path (path_id, start, finish, distance, route_id)
--values (1, '�������� ����', '���� ��. ��������', 35, 1), (2, '������i�����', '���������', 50, 2), (3, '������������', '���������', 100, 3), (4, '�������� ����', '���� ��. ��������', 42, 4)
values (5, '���� 1', '���� 2', 35, 1), (6, '���� 2', '���� 3', 50, 2), (7, '���� 3', '���� 4', 100, 3), (8, '���� 4', '���� 5', 42, 4)

insert into transport_name (transport_id, name, route_id, avg_speed, coast, num)
--values (1, '���������', 1, 75, 8, 50), (2, '�������', 2, 55, 8, 20), (3, '�������', 3, 55, 8, 30), (4, '�������', 4, 55, 8, 30)
values (5, '�������', 1, 65, 6, 5), (6, '�������', 2, 65, 6, 6), (7, '�������', 3, 65, 6, 5), (8, '�������', 4, 65, 6, 9)

select * from path
select * from transport_name
select * from route


-- ���i��� 1
select MIN(60 * a.distance / b.avg_speed)
from path a
inner join transport_name b 
on a.path_id = b.transport_id
where start = '�������� ����'

GO

-- ���i��� 2
CREATE VIEW avgWaitingTime (WaitingTime) AS
select (60 * a.distance / b.avg_speed) / c.num_of_stops
from path a
inner join transport_name b
on a.path_id = b.transport_id
inner join  route c
on a.path_id = c.route_id
where c.route_id = 2

GO

-- ���i��� 3
CREATE PROC TramPaths AS
select a.name, b.distance
from transport_name a
inner join path b
on a.transport_id = b.path_id
where a.name = '�������'
order by b.distance desc

GO

-- ���i��� 4
create proc PributokZaDen @transport nvarchar(20), @pributok int as
select a.transport_id, a.name, a.coast * b.num_of_stops * b.num_of_cars * b.num_of_passangers
from transport_name a
inner join route b
on a.route_id = b.route_id
